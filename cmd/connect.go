/*
Copyright © 2020 Vincent Bloom @ ByLight <vincent.bloom@bylight.com>
Licensed by ByLight PCTE Distribution

*/
package cmd

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"github.com/spf13/viper"
	"github.com/zalando/go-keyring"

	"bitbucket.org/vincent-bloom/giraffe/internal/utils"
	"github.com/spf13/cobra"
)

var zones = []string{
	"dev00",
	"dev01",
	"dev02",
	"dev03",
	"dev04",
	"dev05",
	"dev06",
	"dev07",
	"dev08",
	"dev09",
	"test00",
	"test01",
	"test02",
	"test03",
	"qa00",
	"qa01",
	"qa02",
	"qa03",
}

// connectCmd represents the connect command
var connectCmd = &cobra.Command{
	Use:   "connect",
	Short: "hook up to the vendor vpn",
	Long: `Connect to RCS. Through the command, prompts 
for One-Time-Password delivered to you via email. 
It then opens up the f5vpn application and connects to vpn.`,
	Args:      cobra.OnlyValidArgs,
	ValidArgs: zones,
	Run: func(cmd *cobra.Command, args []string) {

		view, err := cmd.Flags().GetBool("view")
		if err != nil {
			panic(err)
		}

		recreate, err := cmd.Flags().GetBool("recreate")
		if err != nil {
			panic(err)
		}

		// TODO: Check the docker container instead of doing manual sleep
		utils.Start(view, recreate)
		time.Sleep(5 * time.Second)

		vpn := viper.GetString("vpn.vendor.rcs")
		user := viper.GetString("vpn.vendor.user")

		if vpn == "" {
			fmt.Println("vpn.vendor.rcs not found in config. Use 'giraffe config` to store connection information")
			vpn = "rcs01"
		}

		if user == "" {
			fmt.Println("vpn.vendor.user not found in config. Use 'giraffe config` to store connection information")

			// Gather user
			scanner := bufio.NewScanner(os.Stdin)
			fmt.Printf("Enter %s username: ", vpn)
			scanner.Scan()
			user = scanner.Text()
		}

		if _, err := keyring.Get(vpn, user); err != nil {
			fmt.Println("Secret not found in keyring. Use 'giraffe config` to store your password in keyring")
		}

		if len(args) > 0 {
			utils.Connect(view, vpn, user, args[0])
		} else {
			fmt.Printf("Using default zone 'dev01'\n")
			utils.Connect(view, vpn, user, "dev01")
		}

	},
}

func init() {
	rootCmd.AddCommand(connectCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// connectCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// connectCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	connectCmd.Flags().BoolP("toggle", "t", false, "help message for toggle")
	connectCmd.Flags().BoolP("view", "v", false, `view the connection output. Please install 
  https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.20.529-Linux-x64.deb 
and connect to 127.0.0.1:5900 through the vnc app`)
	connectCmd.Flags().BoolP("recreate", "", false, "recreate selenium container")
}
