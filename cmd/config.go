/*
Copyright © 2020 Vincent Bloom @ ByLight <vincent.bloom@bylight.com>
Licensed by ByLight PCTE Distribution

*/
package cmd

import (
	"bufio"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/zalando/go-keyring"
	"golang.org/x/crypto/ssh/terminal"
)

var vpn = "rcs01"

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "set up credentials for " + vpn,
	Long: `Runs you through the configuration needed to automate connecting to vendor-vpn.

	Run before using connect command.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Set vpn name in config
		viper.Set("vpn.vendor.rcs", vpn)

		// Gather username
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Printf("Enter %s username: ", vpn)
		scanner.Scan()
		user := scanner.Text()

		// Set username in config
		viper.Set("vpn.vendor.user", user)

		// Gather password
		fmt.Printf("Enter %s password: ", vpn)
		password, _ := terminal.ReadPassword(0)
		pass := string(password[:])

		// Re-read the user from config to make sure it works
		user = viper.GetString("vpn.vendor.user")

		// Save password in keyring
		if err := keyring.Set(vpn, user, pass); err != nil {
			panic(err)
		}

		// Persist vendor-vpn information
		if err := viper.WriteConfig(); err != nil {
			panic(err)
		}

		fmt.Printf("\nSuccessfully saved %s's password for %s in Login keyring", user, vpn)
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// configCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// configCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
