Giraffe
=========================================

This project aims to aid developers by setting up a common development environment.
It will also help the devops team delivering Metova products.

## Prerequisites

* Go 1.14.6

Go installation instructions:

```shell
# Download and extract Go 1.14.6. 
# For correct distribution, check: https://golang.org/dl/#go1.14.6
curl -o go1.14.6.tar.gz -L https://golang.org/dl/go1.14.6.linux-amd64.tar.gz
tar -zcvf go1.14.6.tar.gz

# Make go's bin available to all profiles in system.
mv go /usr/local

# Add go/bin to system $PATH.
echo 'export PATH=$PATH:/usr/local/go/bin' >> /etc/profile

# Add $HOME/go/bin to user $PATH.
echo 'export PATH=$PATH:$HOME/go/bin' >> $HOME/.bashrc

# Verify installation (after reloading shell or sourcing profile).
go version
# $ go version
# go version go1.14.6 linux/amd64

# View Go's environment
go env
```

A few notes about `$GOPATH`, `$GOBIN` and `$GOROOT`:

* `$GOPATH` is the root of the workspace. Since Go 1.8, if `$GOPATH` environment variable is not set. This defaults to `$HOME/go` on Unix/Linux and `%USERPROFILE%/go` on Windows[^1].
* `$GOBIN` is directory where `go install` and `go get` will place binaries after building main packages. Generally this is set to somewhere on the system `$PATH` so that installed binaries can be run and discovered easily. This defaults to `$GOPATH/bin`.
* `$GOROOT` is the Go installation location. It is used to find the standard libraries. In this case, it will be set to `/usr/local/go`.

## Installation

After installing Go, this project can be installed in a couple of ways. 

Via Go:
```shell
go get bitbucket.org/vincent-bloom/giraffe
```

Via Git:
```shell
git clone git@bitbucket.org:vincent-bloom/giraffe.git
cd giraffe
go install .
```

To test either installation (assumming `go/bin` is set in `$PATH`):

```shell
giraffe
```

## Developing

### Adding a CLI command

#### Configuring Cobra

This project is built using Cobra, a tool created to facilitate Go CLI creation{^2]. You must first configure Cobra to correctly generate headers in created command files. You can configure Cobra by copying `.cobra.yml` configuration file to your home directory, then editing the values.

```shell
cp .cobra.yml $HOME
vi $HOME/.cobra.yml
```

#### Generating a command

To add a new command using the default cobra config:

```shell
cobra add <command> --config .cobra.yml
```

To add a new command using your personal cobra config (located in `$HOME/.cobra.yml`)[^3]:

```shell
cobra add <command>
```

This will generate a new command in `cmd/<command>.go` and all the resources to run the command necessary.
So then, you'll now have access to:

```shell
giraffe <command>
```

To add a child command, such as `giraffe <command> <subcommand>`:

```shell
cobra add <subcommand> -p <command>
```

You can now edit the newly added command inside the `cmd` folder:

```shell
vi cmd/<subcommand>.go
```

## Removal

To remove this binary, run:
```shell
go clean -i bitbucket.org/vincent-bloom/giraffe
```

## Contributing

1. Create a branch.
2. Make changes and commits to created branch.
  * If reaching a sharable, stable point in code, create git tags with the format: `v0.0.0-yyyymmddhhmm`.  
3. Bump version in `cmd/root.go`.
4. Create a merge request.
5. After merge request is approved, create a new tag `v<major>.<minor>.<patch>` corresponding with the merge commit.

## TODOs

### General
* Refactor `rcs_connect.go`.
* Connect to metova vpn.
* Disconnect from metova vpn.
* Correctly open f5vpn app with retries.

### CLI
* Add disconnect function.
* Add a default zone to config.
* Clean up cli help.
* Add metova as a connect arg.

### Selenium
* Add flag to autoremove selenium container after succesful vpn connection.
* Orchestrate multiple selenium sessions to avoid multiple logins if zone already available. This involves playing with the selenium Dockerfile.
* Potentially change selenium docker networking from host to bridge.

## Known bugs
* Opening f5vpn sometimes fails, and the process remains as a zombie for over 30 seconds. This will report as a false positive to the `connect` command, triggering an early exit.

## Authors
* Vincent Bloom (vincent.bloom@bylight.com).

## Further reading
* [Go Versioning Information](https://research.swtch.com/vgo-intro).
* [Publishing Go Modules](https://blog.golang.org/publishing-go-modules).

[^1]: [Command go: GOPATH environment variable](https://golang.org/cmd/go/#hdr-GOPATH_environment_variable).
[^2]: [Cobra](https://github.com/spf13/cobra).
[^3]: [Cobra Generator](https://github.com/spf13/cobra/blob/master/cobra/README.md).
