package utils

import (

	// 	"fmt"
	// 	"log"
	// 	"os"
	// 	"path/filepath"
	// 	"strings"

	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"time"

	// 	"github.com/docker/docker/api/types"

	"github.com/docker/docker/api/types"
	container "github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/mount"
	client "github.com/docker/docker/client"
	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/docker/docker/pkg/term"
	nat "github.com/docker/go-connections/nat"
	gigi "github.com/mitchellh/go-ps"
	"github.com/tebeka/selenium"
	"github.com/zalando/go-keyring"
	"golang.org/x/crypto/ssh/terminal"
)

var seleniumName = "giraffe-selenium"
var seleniumImage = "selenium/standalone-firefox:3.141.59-20200730"
var zoneLabels = map[string]string{
	"dev01":  "input_1_0",
	"dev02":  "input_1_1",
	"dev03":  "input_1_2",
	"dev04":  "input_1_3",
	"dev05":  "input_1_4",
	"dev06":  "input_1_5",
	"dev07":  "input_1_6",
	"dev08":  "input_1_7",
	"test01": "input_1_8",
	"test02": "input_1_9",
	"qa01":   "input_1_10",
	"qa02":   "input_1_11",
}

// Connect creates a non-interactive session to f5 to connect to vendor-vpn
func Connect(view bool, rcs, user, zone string) {

	// Connect to the selenium server
	// selenium.SetDebug(true)
	caps := selenium.Capabilities{"browserName": "firefox", "browserVersion": "79.0", "acceptInsecureCerts": true, "ensureCleanSession": true}
	//     wd, err := selenium.NewRemote(caps, "http://127.0.0.1:4444")

	log.Println("Connecting to selenium container...")
	wd, err := selenium.NewRemote(caps, fmt.Sprintf("http://127.0.0.1:%d/wd/hub", 4444))

	if err != nil {
		fmt.Println(err)
	}

	defer wd.Quit()

	log.Println("Connected to selenium container")

	log.Println("Connecting to https://rcs01v.pcte.mil...")
	if wd.Get("https://rcs01v.pcte.mil") != nil {
		// Ocasionally the page needs to be refreshed
		log.Println("Attempting again...")
		if wd.Refresh() != nil {
			log.Panicln(`Unable to reach https://rcs01v.pcte.mil. Try running 

  giraffe connect --recreate ` + zone + `

`)
		}
	}

	log.Println("Found webtop")

	//
	// DoD Warning and Consent Banner
	//

	if banner, err := wd.FindElement(selenium.ByID, "faultbox"); err == nil {
		text, _ := banner.Text()
		fmt.Println(text)
	} else {
		panic(err)
	}

	if ul, err := wd.FindElements(selenium.ByXPATH, "/html/body/div/div/div[3]/ul/li"); err == nil {
		for _, li := range ul {
			text, _ := li.Text()
			fmt.Printf(" * %s\n", text)
		}
	} else {
		panic(err)
	}

	if okay, err := wd.FindElement(selenium.ByID, "btn"); err == nil {
		okay.Click()
	} else {
		panic(err)
	}

	//
	// Please choose one of the following two options below.
	//

	// This selects Non-CAC 2FA
	// TODO: authentication via CAC ?
	if auth, err := wd.FindElement(selenium.ByLinkText, "Non-CAC 2FA Authentication"); err == nil {
		auth.Click()
		time.Sleep(1 * time.Second)
	} else {
		panic(err)
	}

	//
	// Username / password page
	//

	if usernameField, err := wd.FindElement(selenium.ByID, "input_1"); err == nil {
		// If user wasn't set in config, prompt for username
		if user == "" {
			panic("User not supplied!")
		}

		usernameField.SendKeys(user)
	} else {
		panic(err)
	}

	if passwordField, err := wd.FindElement(selenium.ByID, "input_2"); err == nil {
		// Get password from keyring
		password, err := keyring.Get(rcs, user)

		// If password not foud, prompt for password
		if err != nil {
			fmt.Printf("Enter %s password: ", rcs)
			if pass, err := terminal.ReadPassword(0); err != nil {
				password = string(pass[:])
			} else {
				panic(err)
			}
		}

		passwordField.SendKeys(password)
	} else {
		panic(err)
	}

	if submit, err := wd.FindElement(selenium.ByCSSSelector, ".credentials_input_submit"); err == nil {
		submit.Click()
		time.Sleep(1 * time.Second)
	} else {
		panic(err)
	}

	//
	// A one-time passcode has been emailed to the email address registered to your account.
	// Please check your email and provide the code below
	//
	if !view {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter OTP: ")
		text, _ := reader.ReadString('\n')
		otpField, _ := wd.FindElement(selenium.ByID, "input_2")
		otpField.SendKeys(text)
	} else {
		// Manually enter the code into the VNC viewer. Don't hit Submit.
		time.Sleep(45 * time.Second)
	}

	if submit, err := wd.FindElement(selenium.ByCSSSelector, ".credentials_input_submit"); err == nil {
		submit.Click()
		time.Sleep(1 * time.Second)
	} else {
		panic(err)
	}

	//
	// Please select which network you want to access
	//
	if zone, err := wd.FindElement(selenium.ByID, zoneLabels[zone]); err == nil {
		zone.Click()
	} else {
		panic(err)
	}

	if submit, err := wd.FindElement(selenium.ByCSSSelector, ".credentials_input_submit"); err == nil {
		submit.Click()
		time.Sleep(1 * time.Second)
	} else {
		panic(err)
	}

	//
	// f5-vpn download link
	//

	// the magic link is stored in a field of a variable the document root, so this script fetches it
	if result, err := wd.ExecuteScript("return g_CustomProtocolHandler.options.link", nil); err == nil {
		fmt.Printf("Magic link: %s\n", result.(string))

		if processes, err := gigi.Processes(); err == nil {
			for _, k := range processes {
				if k.Executable() == "f5vpn" {
					process, err := os.FindProcess(k.Pid())
					if err != nil {
						panic(err)
					}
					// TODO: prompt to kill previous process?
					// TODO: kill metova vpn in ppp0
					fmt.Printf("Killing previous f5 seession PID [%d]\n", k.Pid())
					process.Kill()
					break
				}
			}
		} else {
			panic(err)
		}

		fmt.Printf(`Attempting to open f5. Please note that this may fail and not report correctly. If that
happens, copy the magic link and manually rerun:

  /opt/f5/vpn/f5vpn "` + result.(string) + `" &

`)
		for i, retry := 1, true; i < 6 && retry == true; i++ {
			fmt.Printf("Attempt %d/5...\n", i)

			cmd := exec.Command("/opt/f5/vpn/f5vpn", result.(string))
			if cmd.Start() != nil {
				panic(err)
			}

			time.Sleep(45 * time.Second)
			// TODO: Do a different verification method
			// Sometimes it takes it a long time to open, and even after that time it may fail
			if processes, err := gigi.Processes(); err == nil {
				for _, k := range processes {
					if k.Executable() == "f5vpn" {
						fmt.Printf("Found f5: %s [%d]\n", k.Executable(), k.Pid())
						retry = false
						break
					}
				}
				if retry {
					fmt.Println("Skipping...")
				}
			} else {
				panic(err)
			}
		}

	} else {
		panic(err)
	}
}

// Start the selenium server
func Start(view, recreate bool) {
	var re = regexp.MustCompile(`^(.+):(.+)$`)
	seleniumImage = re.ReplaceAllString(seleniumImage, "$1-debug:$2")

	image := checkSeleniumImage()
	if !image {
		fmt.Printf("Pulling selenium docker image: %s", seleniumImage)
		pullSeleniumImage()
	}

	container := checkSeleniumContainer()

	var cp string

	if container == "" || recreate {
		deleteSeleniumContainers()
		cp, _ = createSeleniumContainer(view)
	} else {
		cp = checkSeleniumContainer()
	}

	startSeleniumContainer(cp)
}

func testKill() {
	processes, err := gigi.Processes()

	if err != nil {
		panic(err)
	}

	fmt.Printf("Processes: %d\n", len(processes))

	for _, k := range processes {
		if k.Executable() == "sleep" {
			fmt.Printf("Processes found: %d\n", k.Pid())
			process, err := os.FindProcess(k.Pid())
			if err != nil {
				panic(err)
			}
			process.Kill()
			break
		}
	}

	processes, err = gigi.Processes()

	if err != nil {
		panic(err)
	}

	fmt.Printf("Processes after kill: %d\n", len(processes))

	for _, k := range processes {
		if k.Executable() == "sleep" {
			fmt.Printf("Processes found: %d\n", k.Pid())
			process, err := os.FindProcess(k.Pid())
			if err != nil {
				panic(err)
			}
			process.Kill()
			break
		}
	}

	fmt.Printf("Processes after second kill: %d\n", len(processes))

}

func checkSeleniumContainer() string {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{
		All:     true,
		Filters: filters.NewArgs(filters.Arg("name", "^/"+seleniumName+"$")),
	})

	if err != nil {
		fmt.Printf("Error: %s", err.Error())
		panic(err)
	}

	for _, container := range containers {
		// fmt.Printf("%s\n", container.Names)
		return container.ID
	}

	return ""
}

func createSeleniumContainer(debugView bool) (string, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	config := &container.Config{
		Image: seleniumImage,
		ExposedPorts: nat.PortSet{
			nat.Port("4444/tcp"): {},
		},
	}

	// TODO: add healthchecks
	hostConfig := &container.HostConfig{
		DNS:         []string{"192.168.0.1", "10.1.20.14"},
		NetworkMode: "host",
		// PortBindings: nat.PortMap{
		// 	nat.Port("4444/tcp"): []nat.PortBinding{{HostIP: "0.0.0.0", HostPort: "4444"}},
		// },
	}

	if debugView {
		var emptyStruct struct{}
		config.ExposedPorts[nat.Port("5900/tcp")] = emptyStruct
		//		hostConfig.PortBindings[nat.Port("5900/tcp")] = []nat.PortBinding{{HostIP: "0.0.0.0", HostPort: "5900"}}
		hostConfig.Mounts = []mount.Mount{
			{
				Type:   mount.TypeBind,
				Source: "/dev/shm",
				Target: "/dev/shm",
			},
		}
	}

	ctx := context.Background()
	containerResp, err := cli.ContainerCreate(ctx, config, hostConfig, nil, seleniumName)

	if err != nil {
		panic(err)
	}

	return containerResp.ID, err
}

func stopSeleniumContainer() {
	cli, err1 := client.NewClientWithOpts(client.FromEnv)
	if err1 != nil {
		panic(err1)
	}

	ctx := context.Background()

	containers, err2 := cli.ContainerList(ctx, types.ContainerListOptions{
		All:     true,
		Filters: filters.NewArgs(filters.Arg("name", "^/"+seleniumName+"$")),
	})

	if err2 != nil {
		panic(err2)
	}

	if len(containers) > 0 {
		err3 := cli.ContainerRemove(ctx, containers[0].ID, types.ContainerRemoveOptions{
			RemoveVolumes: true,
		})

		if err3 != nil {
			panic(err3)
		}
	}
}

func deleteSeleniumContainers() {
	cli, err1 := client.NewClientWithOpts(client.FromEnv)
	if err1 != nil {
		panic(err1)
	}

	ctx := context.Background()

	containers, err2 := cli.ContainerList(ctx, types.ContainerListOptions{
		All:     true,
		Filters: filters.NewArgs(filters.Arg("name", "^/"+seleniumName+"$")),
	})

	if err2 != nil {
		panic(err2)
	}

	if len(containers) > 0 {
		if containers[0].State != "stopped" {
			t, _ := time.ParseDuration("1m")
			cli.ContainerStop(ctx, containers[0].ID, &t)
		}

		err3 := cli.ContainerRemove(ctx, containers[0].ID, types.ContainerRemoveOptions{
			RemoveVolumes: true,
			Force:         true,
		})

		if err3 != nil {
			panic(err3)
		}
	}
}

func findSeleniumContainers() {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{All: true})
	if err != nil {
		panic(err)
	}

	for _, container := range containers {
		fmt.Printf("%s %s\n", container.ID[:10], container.Image)
	}
}

func startSeleniumContainer(id string) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	//func (cli *Client) ContainerStart(ctx context.Context, containerID string, options types.ContainerStartOptions) error
	err = cli.ContainerStart(ctx, id, types.ContainerStartOptions{})

	if err != nil {
		fmt.Printf("SUCCESS!")
	}
}

func checkSeleniumImage() bool {
	// func (cli *Client) ImageList(ctx context.Context, options types.ImageListOptions) ([]types.ImageSummary, error)
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	image, _ := cli.ImageList(ctx, types.ImageListOptions{
		Filters: filters.NewArgs(filters.Arg("reference", seleniumImage)),
	})

	return len(image) == 1
}

func pullSeleniumImage() {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	// func (cli *Client) ImagePull(ctx context.Context, refStr string, options types.ImagePullOptions) (io.ReadCloser, error)
	pusher, err := cli.ImagePull(ctx, seleniumImage, types.ImagePullOptions{})

	if err != nil {
		panic(err)
	}

	defer pusher.Close()

	termFd, isTerm := term.GetFdInfo(os.Stdout)
	jsonmessage.DisplayJSONMessagesStream(pusher, os.Stdout, termFd, isTerm, nil)
}
