/*
Copyright © 2020 Vincent Bloom @ ByLight <vincent.bloom@bylight.com>
Licensed by ByLight PCTE Distribution

*/
package main

import "bitbucket.org/vincent-bloom/giraffe/cmd"

func main() {
	cmd.Execute()
}
